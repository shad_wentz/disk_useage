import folderstats
import matplotlib.pyplot as plt
import squarify
import sys

# Set the font dictionaries (for plot title and axis titles)
axis_font = {'size':'10'}

folder_name = sys.argv[1]

df = folderstats.folderstats(folder_name, ignore_hidden=True)

with plt.style.context('ggplot'):
    # Group by extension and sum all sizes for each extension 
    extension_sizes = df.groupby('extension')['size'].sum()
    # Sort elements by size
    extension_sizes = extension_sizes.sort_values(ascending=False)
    
    extension_sizes[:20].plot(
        kind='bar', color='C1', title='Extension Distribution by Size');


# Group by extension and sum all sizes for each extension
# extension_sizes = df.groupby('extension')['size'].sum()
# # Sort elements by size
# extension_sizes = extension_sizes.sort_values(ascending=False)

# squarify.plot(sizes=extension_sizes.values, label=extension_sizes.index.values)
# plt.title('Extension Treemap by Size')
# plt.axis('off');

plt.rc('xtick',labelsize=8)
plt.rc('ytick',labelsize=18)

plt.savefig('folder_stats.pdf')
